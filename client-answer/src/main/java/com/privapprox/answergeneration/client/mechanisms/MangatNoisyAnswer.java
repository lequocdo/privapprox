package com.privapprox.answergeneration.client.mechanisms;

import com.privapprox.answergeneration.client.EpsilonCalculation;
import com.privapprox.answergeneration.client.NoisyAnswer;


public class MangatNoisyAnswer implements NoisyAnswer, EpsilonCalculation {

    public boolean getNoisyAnswer(boolean actualAnswer, double flip1_probability, double flip2_probability) {
        FlipCalculation flipCalculation = new FlipCalculation(flip1_probability, flip2_probability);

        int flip1 = flipCalculation.getFlip1();
        int flip2 = flipCalculation.getFlip2();

        if (actualAnswer) {
            return actualAnswer;
        }
        if (flip1 == 1) {
            return actualAnswer;
        } else {
            return !actualAnswer;
        }
    }


    public double epsilon(double flip1_probability, double flip2_probability) {
        double top = 1;
        double bottom = 1 - flip1_probability;
        return top / bottom;
    }
}
