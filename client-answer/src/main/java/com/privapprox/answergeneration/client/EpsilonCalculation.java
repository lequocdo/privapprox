package com.privapprox.answergeneration.client;

public interface EpsilonCalculation {
    double epsilon(double flip1_probability, double flip2_probability);
}
