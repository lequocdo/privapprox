package com.privapprox.answergeneration.client.mechanisms;

import com.privapprox.answergeneration.client.EpsilonCalculation;
import com.privapprox.answergeneration.client.NoisyAnswer;


public class DroginNoisyAnswer implements NoisyAnswer, EpsilonCalculation {
    public boolean getNoisyAnswer(boolean actualAnswer, double flip1_probability, double flip2_probability) {
        FlipCalculation flipCalculation = new FlipCalculation(flip1_probability, flip2_probability);

        if (actualAnswer && flipCalculation.isIsflip1()) {
            return true;
        } else if (actualAnswer && !flipCalculation.isIsflip1()) {
            return false;
        } else if (!actualAnswer && flipCalculation.isIsflip2()) {
            return false;
        } else if (!actualAnswer && !flipCalculation.isIsflip2()) {
            return true;
        }

        throw new IllegalStateException("somehow reach here!");
    }


    public double epsilon(double flip1_probability, double flip2_probability) {
        double top = flip1_probability;
        double bottom = 1 - flip1_probability;
        return top / bottom;
    }
}
