package com.privapprox.answergeneration.client;

import com.pshdd.message.Answer;
import com.pshdd.message.QueryAnswerWithActual;
import com.privapprox.encryption.GoldwasserMicali;
import com.privapprox.encryption.Paillier;
import com.privapprox.encryption.RSA;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.math3.distribution.EnumeratedIntegerDistribution;
import org.apache.thrift.TException;
import org.apache.thrift.TSerializer;
import org.apache.thrift.protocol.TBinaryProtocol;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.*;


public class AnswerGeneration {
    private final SecureRandom random = new SecureRandom();

    public List<Boolean> getAnswers_random(double yes_probability, int bucket_size) {
        List<Boolean> actual_answers = new ArrayList<Boolean>();
        int[] actual_values = new int[]{0, 1};
        double[] actual_probabilites = new double[]{1 - yes_probability, yes_probability};

        EnumeratedIntegerDistribution actual_distribution =
                new EnumeratedIntegerDistribution(actual_values, actual_probabilites);

        int[] actual = actual_distribution.sample(bucket_size);

        for (int x = 0; x < bucket_size; x++) {
            if (actual[x] == 1) {
                actual_answers.add(true);
            } else {
                actual_answers.add(false);
            }
        }
        return actual_answers;
    }

    public Answers getAnswers(double flip1_probability, double flip2_probability,
                              double yes_probability, int bucket_size, NoisyAnswer noisyAnswer) {
        int number_of_answers = bucket_size;
        List<Boolean> noisy_answers = new ArrayList<Boolean>();
        List<Boolean> actual_answers = new ArrayList<Boolean>();

        //double yes_probability=0.3;
        int[] actual_values = new int[]{0, 1};
        double[] actual_probabilites = new double[]{1 - yes_probability, yes_probability};

        EnumeratedIntegerDistribution actual_distribution =
                new EnumeratedIntegerDistribution(actual_values, actual_probabilites);

        int numSamples = 1;
        int[] actual = actual_distribution.sample(numSamples);

        for (int x = 0; x < number_of_answers; x++) {
            boolean actual_answer = false;
            if (actual[0] == 1) {
                actual_answer = true;
            }
            //boolean actual_answer=random.nextBoolean();
            actual_answers.add(actual_answer);
            //boolean noisy_answer=getNoisyAnswer_wiki(actual_answer,flip1_probability,flip2_probability);
            //boolean noisy_answer=getNoisyAnswer_innocous(actual_answer,flip1_probability,flip2_probability);
            //boolean noisy_answer=getNoisyAnswer_instantaneous(actual_answer,flip1_probability,flip2_probability);
            boolean noisy_answer = noisyAnswer.getNoisyAnswer(actual_answer, flip1_probability, flip2_probability);
            noisy_answers.add(noisy_answer);
        }

        Answers answers = new Answers();
        answers.setNoisy_answers(noisy_answers);
        answers.setActual_answers(actual_answers);
        return answers;
    }

    public List<String> getAnswers_GM(List<Boolean> answers) {
        List<String> encryptedAnswers = new ArrayList<String>();
        for (boolean answer : answers) {
            GoldwasserMicali goldwasserMicali = new GoldwasserMicali();
            BigInteger encryptedValue = goldwasserMicali.encrypt(answer);
            String stringEncryptedValue = encryptedValue.toString();
            encryptedAnswers.add(stringEncryptedValue);
        }
        return encryptedAnswers;
    }

    public List<String> getAnswers_Paillier(List<Boolean> answers) throws NoSuchAlgorithmException, BadPaddingException, NoSuchPaddingException, IllegalBlockSizeException, InvalidKeyException {
        List<String> encryptedAnswers = new ArrayList<String>();
        for (boolean answer : answers) {
            Paillier paillier = new Paillier();
            BigInteger m1 = new BigInteger("0");
            if (answer) {
                m1 = new BigInteger("1");
            }
            BigInteger em1 = paillier.Encryption(m1);
            String encryptedValue = new String(em1 + "");
            encryptedAnswers.add(encryptedValue);
        }
        return encryptedAnswers;
    }

    public String encrypted_answers(Answer answer, List<String> encryptedanswers) throws TException {
        List<ByteBuffer> buf = new ArrayList<ByteBuffer>();
        for (String cur : encryptedanswers) {
            ByteBuffer b = ByteBuffer.allocate(cur.length());
            b.put(cur.getBytes());
            buf.add(b);
        }
        answer.answerBits = new ArrayList<ByteBuffer>();
        answer.answerBits.addAll(buf);

        TSerializer serializer = new TSerializer(new TBinaryProtocol.Factory());
        byte[] bytes1 = serializer.serialize(answer);
        String value1 = new String(Base64.encodeBase64(bytes1));
        return value1;
    }

    public String gm_answer(Answer answer, List<Boolean> answers) throws TException {
        List<String> encryptedanswers = getAnswers_GM(answers);
        return encrypted_answers(answer, encryptedanswers);
    }

    public String paillier_answer(Answer answer, List<Boolean> answers) throws TException, IllegalBlockSizeException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException {
        List<String> encryptedanswers = getAnswers_Paillier(answers);
        return encrypted_answers(answer, encryptedanswers);
    }

    public List<ByteBuffer> generate_akkus(int bucket_size) throws Exception {
        RSA rsa = new RSA();
        rsa.init();
        List<ByteBuffer> ret = new ArrayList<ByteBuffer>();
        for (int i = 0; i < bucket_size; i++) {
            byte[] plaintext = new byte[1];
            random.nextBytes(plaintext);
            byte[] encrypted = rsa.encrypt(plaintext);
            ByteBuffer buf = ByteBuffer.allocate(encrypted.length);
            buf.put(encrypted);
            ret.add(buf);
        }
        return ret;
    }

    public String akkus_1(Answer answer) throws Exception {
        List<ByteBuffer> answers = generate_akkus(1);
        answer.setAnswerBits(answers);

        TSerializer serializer = new TSerializer(new TBinaryProtocol.Factory());
        byte[] bytes1 = serializer.serialize(answer);
        String value1 = new String(Base64.encodeBase64(bytes1));
        return value1;
    }

    public String akkus_10(Answer answer) throws Exception {
        List<ByteBuffer> answers = generate_akkus(10);
        answer.setAnswerBits(answers);

        TSerializer serializer = new TSerializer(new TBinaryProtocol.Factory());
        byte[] bytes1 = serializer.serialize(answer);
        String value1 = new String(Base64.encodeBase64(bytes1));
        return value1;
    }

    public String akkus_100(Answer answer) throws Exception {
        List<ByteBuffer> answers = generate_akkus(100);
        answer.setAnswerBits(answers);

        TSerializer serializer = new TSerializer(new TBinaryProtocol.Factory());
        byte[] bytes1 = serializer.serialize(answer);
        String value1 = new String(Base64.encodeBase64(bytes1));
        return value1;
    }

    private class XorEnrypt {
        public byte[] encryptedBytes;
        public byte[] keyBytes;
    }

    public XorEnrypt xorencryptAnswerBits(byte[] plaintext, int bucket_size) {
        final BitSet readonlyplaintextBitSet = QueryAnswerUtil.fromByteArray(plaintext, bucket_size);

        //copy plaintext as want to verify can reconstruct and match plaintext before returning
        BitSet encryptedBitSet = (BitSet) readonlyplaintextBitSet.clone();
        int length = plaintext.length;

        //generate key
        byte[] random_bytes = new byte[length];
        random.nextBytes(random_bytes);
        BitSet keyBitSet = QueryAnswerUtil.fromByteArray(random_bytes, bucket_size);

        //encrypt
        encryptedBitSet.xor(keyBitSet);

        byte[] encryptedBytes = QueryAnswerUtil.toByteArray(encryptedBitSet, bucket_size);
        byte[] keyBytes = QueryAnswerUtil.toByteArray(keyBitSet, bucket_size);

        XorEnrypt xorEnrypt = new XorEnrypt();
        xorEnrypt.encryptedBytes = encryptedBytes;
        xorEnrypt.keyBytes = keyBytes;


        byte[] verifybytes =
                QueryAnswerUtil.xor_bytes(encryptedBytes, keyBytes, bucket_size);

        //sanity check
        BitSet verifyBitSet = QueryAnswerUtil.fromByteArray(verifybytes, bucket_size);
        if (!readonlyplaintextBitSet.equals(verifyBitSet)) {
            System.out.println(readonlyplaintextBitSet);
            System.out.println(verifyBitSet);
            throw new IllegalStateException("xor not matching");
        } else {
            //System.out.println("matching");
        }

        return xorEnrypt;
    }

    public List<String> split_answer(QueryAnswerWithActual queryAnswer, int bucket_size) throws TException {
        QueryAnswerWithActual encrypted_queryanswer = new QueryAnswerWithActual(queryAnswer);
        QueryAnswerWithActual key_queryanswer = new QueryAnswerWithActual(queryAnswer);

        //encrypt analystid
        key_queryanswer.setAnalystId(random.nextLong());
        long tmpanalystid = encrypted_queryanswer.getAnalystId();
        tmpanalystid = tmpanalystid ^ key_queryanswer.getAnalystId();
        encrypted_queryanswer.setAnalystId(tmpanalystid);

        //encrypt queryid
        key_queryanswer.setQueryId(random.nextLong());
        long tmpqueryid = encrypted_queryanswer.getQueryId();
        tmpqueryid = tmpqueryid ^ key_queryanswer.getQueryId();
        encrypted_queryanswer.setQueryId(tmpqueryid);

        //encrypt answer bits
        XorEnrypt xorEnrypt = xorencryptAnswerBits(queryAnswer.getAnswerBits(), bucket_size);
        encrypted_queryanswer.setAnswerBits(xorEnrypt.encryptedBytes);
        key_queryanswer.setAnswerBits(xorEnrypt.keyBytes);

        TSerializer serializer = new TSerializer(new TBinaryProtocol.Factory());
        byte[] bytes1 = serializer.serialize(encrypted_queryanswer);
        String value1 = new String(Base64.encodeBase64(bytes1));

        byte[] bytes2 = serializer.serialize(key_queryanswer);
        String value2 = new String(Base64.encodeBase64(bytes2));

        List<String> answers = Arrays.asList(value1, value2);
        return answers;
    }
}
