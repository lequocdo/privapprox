package com.privapprox.answergeneration.client.singleton;

import java.io.IOException;
import java.util.Properties;


public class ProbabilityMapping {
    final Properties properties = new Properties();

    private ProbabilityMapping() {
        try {
            properties.load(this.getClass().getResourceAsStream("/yesparameters.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static class LazyHolder {
        private static final ProbabilityMapping INSTANCE = new ProbabilityMapping();
    }

    public static double getFlip1(String yes_probability) {
        String val = LazyHolder.INSTANCE.properties.getProperty(yes_probability);
        String flip1 = val.split(",")[0];
        return new Double(flip1);
    }

    public static double getFlip2(String yes_probability) {
        String val = LazyHolder.INSTANCE.properties.getProperty(yes_probability);
        String flip2 = val.split(",")[1];
        return new Double(flip2);
    }
    /*
    public static ProbabilityMapping getInstance() {
        return LazyHolder.INSTANCE;
    }
    */
}
