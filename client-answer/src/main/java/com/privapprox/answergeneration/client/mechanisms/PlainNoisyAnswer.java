package com.privapprox.answergeneration.client.mechanisms;

import com.privapprox.answergeneration.client.NoisyAnswer;


public class PlainNoisyAnswer implements NoisyAnswer {


    public boolean getNoisyAnswer(boolean actualAnswer, double flip1_probability, double flip2_probability) {
        return true;
    }
}
