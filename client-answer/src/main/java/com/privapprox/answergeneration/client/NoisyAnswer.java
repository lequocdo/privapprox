package com.privapprox.answergeneration.client;


public interface NoisyAnswer {
    boolean getNoisyAnswer(boolean actualAnswer, double flip1_probability, double flip2_probability);
}
