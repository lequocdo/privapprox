package com.privapprox.answergeneration.client;

import com.google.common.base.Splitter;
import com.google.common.primitives.Longs;
import com.pshdd.message.QueryAnswer;
import org.apache.commons.lang3.ArrayUtils;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;


public class QueryAnswerUtil {
    private static final SecureRandom random = new SecureRandom();

    public static void main(String[] args) {
        final int BUCKET_SIZE = 5;

        QueryAnswer queryAnswer = new QueryAnswer();

        int length = 11;
        boolean[] messageBools = new boolean[length];
        for (int i = 0; i < length; i++) {
            messageBools[i] = random.nextBoolean();
        }
        boolean[] originalMessageBools = Arrays.copyOf(messageBools, length);
        BitSet messageBitSet = fromBoolean(messageBools);

        boolean[] keyBools = new boolean[length];
        for (int i = 0; i < length; i++) {
            keyBools[i] = random.nextBoolean();
        }
        BitSet keyBitSet = fromBoolean(keyBools);

        messageBitSet.xor(keyBitSet);
        boolean[] verifyBools = fromBitSet(messageBitSet, length);
        boolean isEquals = Arrays.equals(verifyBools, originalMessageBools);
        System.out.println(isEquals);
        System.out.println(Arrays.toString(verifyBools) + "And " + Arrays.toString(originalMessageBools));

        messageBitSet.xor(keyBitSet);
        verifyBools = fromBitSet(messageBitSet, length);
        isEquals = Arrays.equals(verifyBools, originalMessageBools);
        System.out.println(isEquals);


        BitSet original = new BitSet();
        original.set(0);
        original.set(1);
        original.set(10);
        original.set(11);
        original.set(20);

        byte[] bytes = toByteArray(original, BUCKET_SIZE);
        System.out.println("length=" + bytes.length);
        BitSet converted = fromByteArray(bytes, BUCKET_SIZE);
        System.out.println(original);
        System.out.println(converted);
        if (!original.equals(converted)) {
            System.out.println(original);
            System.out.println(converted);
            System.out.println("Not matching conversion bytes");
        }
    }

    public static byte[] xor_bytes(byte[] encrypted, byte[] key, int bucket_size) {
        BitSet rec1BitSet = QueryAnswerUtil.fromByteArray(encrypted, bucket_size);
        BitSet rec2BitSet = QueryAnswerUtil.fromByteArray(key, bucket_size);

        rec1BitSet.xor(rec2BitSet);

        return QueryAnswerUtil.toByteArray(rec1BitSet, bucket_size);
    }

    public static List<Boolean> xor(List<Boolean> record1, List<Boolean> record2) {
        BitSet rec1BitSet = fromBooleanList(record1);
        BitSet rec2BitSet = fromBooleanList(record2);

        rec1BitSet.xor(rec2BitSet);

        int length = record1.size();
        List<Boolean> xorList = fromBitSetToList(rec1BitSet, length);
        return xorList;
    }

    public static BitSet fromBooleanList(final List<Boolean> messageBooleanList) {
        BitSet bitset = new BitSet();
        int length = messageBooleanList.size();
        int i = 0;
        for (Boolean b : messageBooleanList) {
            if (b) {
                bitset.set(i);
            } else {
                bitset.clear(i);
            }
            i++;
        }
        return bitset;
    }

    public static BitSet fromBooleanObject(final Boolean[] messageBoolean) {
        BitSet bitset = new BitSet();
        for (int i = 0; i < messageBoolean.length; i++) {
            if (messageBoolean[i]) {
                bitset.set(i);
            } else {
                bitset.clear(i);
            }
        }
        return bitset;
    }

    public static BitSet fromBoolean(final boolean[] messageBoolean) {
        Boolean[] messageBooleanObject = ArrayUtils.toObject(messageBoolean);
        return fromBooleanObject(messageBooleanObject);
    }

    public static List<Boolean> fromBitSetToList(final BitSet bitset, int length) {
        boolean[] bools = fromBitSet(bitset, length);
        Boolean[] encryptedObject = ArrayUtils.toObject(bools);
        List<Boolean> ret = Arrays.asList(encryptedObject);
        return ret;
    }

    public static boolean[] fromBitSet(final BitSet bitset, int length) {
        //System.out.println(length);
        boolean[] messageBoolean = new boolean[length];
        for (int i = 0; i < length; i++) {
            boolean val = bitset.get(i);
            messageBoolean[i] = val;
        }
        return messageBoolean;
    }

    public static BitSet fromString(final String bitstring) {
        List<Long> bits = new ArrayList<Long>();
        for (final String token : Splitter.fixedLength(62).split(bitstring)) {
            System.out.println(token);
            Long val = Long.parseLong(token, 2);
            bits.add(val);
        }

        long[] longbits = Longs.toArray(bits);
        return BitSet.valueOf(longbits);
    }

    public static byte[] toByteArray(BitSet bits, int bucketsize) {
        int bytearraylength = (bucketsize + 7) / 8;
        //System.out.println("bytearraylength="+bytearraylength);
        byte[] bytes = new byte[bytearraylength];
        Arrays.fill(bytes, (byte) 0);
        for (int i = 0; i < bucketsize; i++) {
            if (bits.get(i)) {
                //int index=bytes.length-(i/8)-1;
                int index = i / 8;
                //System.out.println("byte index="+index);
                bytes[index] |= 1 << (i % 8);
            }
        }
        return bytes;
    }

    public static BitSet fromByteArray(byte[] bytes, int bucket_size) {
        BitSet bs = new BitSet();
        for (int i = 0; i < bucket_size; i++) {
            int index = i / 8;
            int val = (bytes[index] & (1 << (i % 8)));
            if (val > 0) {
                //System.out.println("i="+i+" index="+index+" val="+val);
                bs.set(i);
            }
        }
        return bs;
    }

    public static byte[] fromBooleanListToBytes(List<Boolean> answers) {
        BitSet bs = fromBooleanList(answers);
        int length = answers.size();
        byte[] bytes = toByteArray(bs, length);
        return bytes;
    }
}
