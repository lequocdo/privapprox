package com.privapprox.answergeneration.client.mechanisms;

import com.privapprox.answergeneration.client.EpsilonCalculation;
import com.privapprox.answergeneration.client.NoisyAnswer;
import org.apache.commons.math3.distribution.EnumeratedIntegerDistribution;


public class InstantaneousNoisyAnswer implements NoisyAnswer, EpsilonCalculation {

    public boolean getNoisyAnswer(boolean actualAnswer, double flip1_probability, double flip2_probability) {
        int[] flip1_values = new int[]{0, 1};
        double[] flip1_probabilites = new double[]{1 - flip1_probability, flip1_probability};

        EnumeratedIntegerDistribution flip1_distribution =
                new EnumeratedIntegerDistribution(flip1_values, flip1_probabilites);

        int numSamples = 1;
        int[] flip1 = flip1_distribution.sample(numSamples);

        int[] flip2_values = new int[]{0, 1};
        double[] flip2_probabilites = new double[]{1 - flip2_probability, flip2_probability};

        EnumeratedIntegerDistribution flip2_distribution =
                new EnumeratedIntegerDistribution(flip2_values, flip2_probabilites);

        int[] flip2 = flip2_distribution.sample(numSamples);

        if (actualAnswer) {
            if (flip1[0] == 1) {
                return true;
            }
            return false;
        } else {
            if (flip2[0] == 1) {
                return true;
            }
            return false;
        }
    }


    public double epsilon(double flip1, double flip2) {
        double top = flip1;
        double bottom = flip2;
        double epsilon1 = top / bottom;
        double epsilon2 = bottom / top;
        if (epsilon1 > epsilon2) {
            return epsilon1;
        } else {
            return epsilon2;
        }
    }
}
