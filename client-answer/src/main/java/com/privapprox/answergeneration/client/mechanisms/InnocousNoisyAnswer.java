package com.privapprox.answergeneration.client.mechanisms;

import com.privapprox.answergeneration.client.EpsilonCalculation;
import com.privapprox.answergeneration.client.NoisyAnswer;
import org.apache.commons.math3.distribution.EnumeratedIntegerDistribution;


public class InnocousNoisyAnswer implements NoisyAnswer, EpsilonCalculation {

    public boolean getNoisyAnswer(boolean actualAnswer, double flip1_probability, double flip2_probability) {
        int[] flip1_values = new int[]{0, 1};
        double[] flip1_probabilites = new double[]{1 - flip1_probability, flip1_probability};

        EnumeratedIntegerDistribution flip1_distribution =
                new EnumeratedIntegerDistribution(flip1_values, flip1_probabilites);

        int numSamples = 1;
        int[] flip1 = flip1_distribution.sample(numSamples);

        int[] flip2_values = new int[]{0, 1};
        double[] flip2_probabilites = new double[]{1 - flip2_probability, flip2_probability};

        EnumeratedIntegerDistribution flip2_distribution =
                new EnumeratedIntegerDistribution(flip2_values, flip2_probabilites);

        int[] flip2 = flip2_distribution.sample(numSamples);

        if (flip1[0] == 1) {
            return actualAnswer;
        } else {
            if (flip2[0] == 1) {
                return true;
            }
            return false;
        }
    }

    public double epsilon(double flip1, double flip2) {
        double top = flip1 + ((1 - flip1) * flip2);
        double bottom = (1 - flip1) * flip2;
        double epsilon = top / bottom;
        return epsilon;
    }

    public static void main(String[] args) {
        InnocousNoisyAnswer answer = new InnocousNoisyAnswer();
        double epsilon = answer.epsilon(0.001, 0.999);
        System.out.println(epsilon);
    }
}
