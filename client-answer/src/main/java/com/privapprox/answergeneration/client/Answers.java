package com.privapprox.answergeneration.client;

import java.util.List;


public class Answers {
    private List<Boolean> noisy_answers;
    private List<Boolean> actual_answers;

    public List<Boolean> getNoisy_answers() {
        return noisy_answers;
    }

    public void setNoisy_answers(List<Boolean> noisy_answers) {
        this.noisy_answers = noisy_answers;
    }

    public List<Boolean> getActual_answers() {
        return actual_answers;
    }

    public void setActual_answers(List<Boolean> actual_answers) {
        this.actual_answers = actual_answers;
    }
}
