package com.privapprox.answergeneration.client.singleton;

import org.apache.commons.math3.distribution.EnumeratedIntegerDistribution;


public class Flip1DistributionSingleton {
    private final EnumeratedIntegerDistribution actual_distribution;

    private Flip1DistributionSingleton(double flip1_probability) {
        int[] flip1_values = new int[]{0, 1};
        double[] flip1_probabilites = new double[]{1 - flip1_probability, flip1_probability};

        this.actual_distribution =
                new EnumeratedIntegerDistribution(flip1_values, flip1_probabilites);
    }

    private static Flip1DistributionSingleton INSTANCE;


    public static Flip1DistributionSingleton getInstance(double flip1_probability) {
        if (INSTANCE == null) {
            INSTANCE = new Flip1DistributionSingleton(flip1_probability);
        }
        return INSTANCE;
    }

    public EnumeratedIntegerDistribution getEnumeratedIntegerDistribution() {
        return this.actual_distribution;
    }
}
