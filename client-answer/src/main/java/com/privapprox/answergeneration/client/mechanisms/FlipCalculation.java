package com.privapprox.answergeneration.client.mechanisms;

import org.apache.commons.math3.distribution.EnumeratedIntegerDistribution;

public class FlipCalculation {
    private int flip1 = 0;
    private int flip2 = 0;
    private boolean isflip1 = false;
    private boolean isflip2 = false;

    public FlipCalculation(double flip1_probability, double flip2_probability) {
        int[] flip1_values = new int[]{0, 1};
        double[] flip1_probabilites = new double[]{1 - flip1_probability, flip1_probability};

        EnumeratedIntegerDistribution flip1_distribution =
                new EnumeratedIntegerDistribution(flip1_values, flip1_probabilites);

        int numSamples = 1;
        int[] flip1 = flip1_distribution.sample(numSamples);

        int[] flip2_values = new int[]{0, 1};
        double[] flip2_probabilites = new double[]{1 - flip2_probability, flip2_probability};

        EnumeratedIntegerDistribution flip2_distribution =
                new EnumeratedIntegerDistribution(flip2_values, flip2_probabilites);

        int[] flip2 = flip2_distribution.sample(numSamples);

        this.flip1 = flip1[0];
        if (this.flip1 == 1) {
            this.isflip1 = true;
        }
        this.flip2 = flip2[0];
        if (this.flip2 == 1) {
            this.isflip2 = true;
        }
    }

    public int getFlip1() {
        return flip1;
    }

    public int getFlip2() {
        return flip2;
    }

    public boolean isIsflip1() {
        return isflip1;
    }

    public boolean isIsflip2() {
        return isflip2;
    }
}

