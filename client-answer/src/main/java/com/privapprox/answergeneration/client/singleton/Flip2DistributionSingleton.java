package com.privapprox.answergeneration.client.singleton;

import org.apache.commons.math3.distribution.EnumeratedIntegerDistribution;


public class Flip2DistributionSingleton {
    private final EnumeratedIntegerDistribution actual_distribution;

    private Flip2DistributionSingleton(double flip2_probability) {
        int[] flip2_values = new int[]{0, 1};
        double[] flip2_probabilites = new double[]{1 - flip2_probability, flip2_probability};

        this.actual_distribution =
                new EnumeratedIntegerDistribution(flip2_values, flip2_probabilites);
    }

    private static Flip2DistributionSingleton INSTANCE;


    public static Flip2DistributionSingleton getInstance(double flip2_probability) {
        if (INSTANCE == null) {
            INSTANCE = new Flip2DistributionSingleton(flip2_probability);
        }
        return INSTANCE;
    }

    public EnumeratedIntegerDistribution getEnumeratedIntegerDistribution() {
        return this.actual_distribution;
    }
}
