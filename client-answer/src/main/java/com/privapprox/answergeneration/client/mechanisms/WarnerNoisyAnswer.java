package com.privapprox.answergeneration.client.mechanisms;

import com.privapprox.answergeneration.client.EpsilonCalculation;
import com.privapprox.answergeneration.client.NoisyAnswer;
import org.apache.commons.math3.distribution.EnumeratedIntegerDistribution;


public class WarnerNoisyAnswer implements NoisyAnswer, EpsilonCalculation {

    public boolean getNoisyAnswer(boolean actualAnswer, double flip1_probability, double flip2_probability) {
        int[] flip1_values = new int[]{0, 1};
        double[] flip1_probabilites = new double[]{1 - flip1_probability, flip1_probability};

        EnumeratedIntegerDistribution flip1_distribution =
                new EnumeratedIntegerDistribution(flip1_values, flip1_probabilites);

        int numSamples = 1;
        int[] flip1 = flip1_distribution.sample(numSamples);

        if (flip1[0] == 1) {
            return actualAnswer;
        } else {
            return !actualAnswer;
        }
    }


    public double epsilon(double flip1, double flip2) {
        double top = flip1;
        double bottom = 1 - flip1;
        double epsilon = top / bottom;
        return epsilon;
    }
}
