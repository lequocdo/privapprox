package com.privapprox.answergeneration.client.singleton;

import org.apache.commons.math3.distribution.EnumeratedIntegerDistribution;


public class ActualAnswerDistributionSingleton {
    private final EnumeratedIntegerDistribution actual_distribution;

    private ActualAnswerDistributionSingleton(double yes_probability) {
        int[] actual_values = new int[]{0, 1};
        double[] actual_probabilites = new double[]{1 - yes_probability, yes_probability};

        this.actual_distribution =
                new EnumeratedIntegerDistribution(actual_values, actual_probabilites);
    }

    private static ActualAnswerDistributionSingleton INSTANCE;


    public static ActualAnswerDistributionSingleton getInstance(double yes_probability) {
        if (INSTANCE == null) {
            INSTANCE = new ActualAnswerDistributionSingleton(yes_probability);
        }
        return INSTANCE;
    }

    public EnumeratedIntegerDistribution getEnumeratedIntegerDistribution() {
        return this.actual_distribution;
    }
}
