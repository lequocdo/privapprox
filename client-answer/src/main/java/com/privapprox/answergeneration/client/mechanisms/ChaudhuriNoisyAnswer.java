package com.privapprox.answergeneration.client.mechanisms;

import com.privapprox.answergeneration.client.EpsilonCalculation;
import com.privapprox.answergeneration.client.NoisyAnswer;


public class ChaudhuriNoisyAnswer implements NoisyAnswer, EpsilonCalculation {
    public boolean getNoisyAnswer(boolean actualAnswer, double flip1_probability, double flip2_probability) {
        FlipCalculation flipCalculation = new FlipCalculation(flip1_probability, flip2_probability);
        int i = 0;
        if (actualAnswer) {
            i = 1;
        }
        int r = (i - flipCalculation.getFlip2()) / (flipCalculation.getFlip1() - flipCalculation.getFlip2());

        if (r == 1) {
            return true;
        } else if (r == 0) {
            return false;
        }
        throw new IllegalStateException("should not get here!");
    }

    public double epsilon(double flip1_probability, double flip2_probability) {
        return 0;
    }
}
