package com.privapprox.encryption;

import java.math.BigInteger;
import java.security.SecureRandom;

public class GoldwasserMicali {
    private static final BigInteger ZERO = BigInteger.ZERO;
    private static final BigInteger ONE = BigInteger.ONE;
    private static final BigInteger THREE = BigInteger.valueOf(3);
    private static final BigInteger FIVE = BigInteger.valueOf(5);
    private static final BigInteger SEVEN = BigInteger.valueOf(7);

    //Random number generator
    private SecureRandom rand = new SecureRandom();

    //Bitsize: size in bits of p and q, this number must be a power of 2, and >= 8. For 1024 security, bitSize must be 512.
    private int bitSize;

    //Private key
    private BigInteger p, q;
    //Public key
    private BigInteger x, n;

    //default constructor -- have to invoke generateKeys() to initialize the keys afterwards, and then we can explicitly get the private key and public key
    public GoldwasserMicali() {
    }

    //constructor using only bitSize -- implicitly invoke generateKeys() to initialize the keys, so we cannot explicitly get the private key and public key
    public GoldwasserMicali(int bitSize) {
        generateKeys(bitSize);
    }

    //constructor for those who don't need to decrypt messages, i.e., don't need to know the private key
    public GoldwasserMicali(int bitSize, BigInteger x, BigInteger n) {
        this.bitSize = bitSize;
        this.x = x;
        this.n = n;
    }

    //constructor for those who need to decrypt messages, i.e., also need to know the private key
    public GoldwasserMicali(int bitSize, BigInteger p, BigInteger q, BigInteger x, BigInteger n) {
        this.bitSize = bitSize;
        this.p = p;
        this.q = q;
        this.x = x;
        this.n = n;
    }

    //generate keys based on bitSize, the return values are private key and public key
    public BigInteger[] generateKeys(int bitSize) {
        this.bitSize = bitSize;

        p = randomNumber(true, bitSize);
        do {
            q = randomNumber(true, bitSize);
        } while (p.equals(q));

        n = p.multiply(q);
        do {
            x = randomNumber(false, (bitSize << 1));
        } while (x.compareTo(n) >= 0 || computeJacobi(x, p) != -1 || computeJacobi(x, q) != -1);

        return new BigInteger[]{p, q, x, n};
    }

    //compute Jacobi symbol (a/n)
    public static int computeJacobi(BigInteger a, BigInteger n) {
        int j = 1;

        BigInteger res, tmp;

        a = a.mod(n);
        while (!a.equals(ZERO)) {
            while (a.and(ONE).equals(ZERO)) {
                a = a.shiftRight(1);

                res = n.and(SEVEN);
                if (res.equals(THREE) || res.equals(FIVE)) {
                    j = -j;
                }
            }

            tmp = a;
            a = n;
            n = tmp;

            if (a.and(THREE).equals(THREE) && n.and(THREE).equals(THREE)) {
                j = -j;
            }

            a = a.mod(n);
        }

        if (n.equals(ONE)) {
            return j;
        } else {
            return 0;
        }
    }

    private BigInteger randomNumber(boolean prime, int size) {
        if (prime) {
            return BigInteger.probablePrime(size, rand);
        }

        BigInteger number;

        byte[] bNumber = new byte[size >> 3];
        do {
            rand.nextBytes(bNumber);
            number = new BigInteger(bNumber);
        } while (number.compareTo(ZERO) <= 0);

        return number;
    }

    public BigInteger encrypt(boolean bit) {
        BigInteger y, y2;
        do {
            y = randomNumber(false, (bitSize << 1));
        } while (y.compareTo(n) >= 0);

        y2 = y.multiply(y).mod(n);
        if (bit) {
            return y2.multiply(x).mod(n);
        } else {
            return y2;
        }
    }

    public boolean decrypt(BigInteger c) {
        return computeJacobi(c, p) == -1;
    }

    public BigInteger xor(BigInteger c1, BigInteger c2) {
        return c1.multiply(c2).mod(n);
    }

    public static void main(String[] args) {
        //GoldwasserMicali gm = new GoldwasserMicali(512);
        GoldwasserMicali gm = new GoldwasserMicali();
        BigInteger[] keys = gm.generateKeys(512);
        for (BigInteger k : keys) {
            System.out.println(k);
        }

        String m = "101011101000011011111000000110101";
        System.out.println(m);

        for (int i = 0; i < m.length(); i++) {
            BigInteger c = gm.encrypt(m.charAt(i) == '1');
            System.out.print((gm.decrypt(c) ? 1 : 0));
        }
        System.out.println();

        BigInteger r1 = gm.encrypt(false);
        for (int i = 0; i < m.length(); i++) {
            BigInteger c1 = gm.encrypt(m.charAt(i) == '1');
            BigInteger c2 = gm.xor(c1, r1);
            System.out.print((gm.decrypt(c2) ? 1 : 0));
        }
        System.out.println();

        BigInteger r2 = gm.encrypt(true);
        for (int i = 0; i < m.length(); i++) {
            BigInteger c1 = gm.encrypt(m.charAt(i) == '1');
            BigInteger c2 = gm.xor(c1, r2);
            System.out.print((gm.decrypt(c2) ? 1 : 0));
        }
        System.out.println();

        //test Jacobi symbol computation (x/n)
        System.out.println(GoldwasserMicali.computeJacobi(keys[2], keys[3]));


        //performance test below
        long t_enc = 0;
        long t_dec = 0;
        long t_xor = 0;

        t_enc -= new java.util.Date().getTime();
        for (int i = 0; i < 100000; ++i) {
            gm.encrypt(false);
        }
        t_enc += new java.util.Date().getTime();

        BigInteger b1 = gm.encrypt(false);
        t_dec -= new java.util.Date().getTime();
        for (int i = 0; i < 10000; ++i) {
            gm.decrypt(b1);
        }
        t_dec += new java.util.Date().getTime();

        BigInteger b2 = gm.encrypt(false);
        t_xor -= new java.util.Date().getTime();
        for (int i = 0; i < 100000; ++i) {
            gm.xor(b1, b2);
        }
        t_xor += new java.util.Date().getTime();

        System.out.println("# encryptions per second = " + String.valueOf(100000 * 1000 / (double) t_enc));
        System.out.println("# decryptions per second = " + String.valueOf(10000 * 1000 / (double) t_dec));
        System.out.println("# XORs per second = " + String.valueOf(100000 * 1000 / (double) t_xor));
    }
}
