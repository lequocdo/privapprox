package com.privapprox.encryption;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import java.security.*;


public class RSA {
    private static final int KEY_LENGTH = 1024;
    private static final String RSA_ECB_PKCS1_MODE = "RSA/ECB/PKCS1PADDING";
    private Key publicKey;
    private Key privateKey;

    public RSA() {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    public void init() throws NoSuchProviderException, NoSuchAlgorithmException {
        KeyPairGenerator keygenerator = KeyPairGenerator.getInstance("RSA", "BC");
        SecureRandom random = new SecureRandom();
        keygenerator.initialize(KEY_LENGTH, random);
        KeyPair pair = keygenerator.generateKeyPair();
        this.publicKey = pair.getPublic();
        this.privateKey = pair.getPrivate();
    }

    public byte[] encrypt(byte[] plaintext) throws InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance(RSA_ECB_PKCS1_MODE);
        cipher.init(Cipher.ENCRYPT_MODE, this.publicKey);
        byte[] encrypted = cipher.doFinal(plaintext);
        return encrypted;
    }

    public byte[] decrypt(byte[] encrypted) throws NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException, InvalidKeyException {
        Cipher cipher = Cipher.getInstance(RSA_ECB_PKCS1_MODE);
        cipher.init(Cipher.DECRYPT_MODE, this.privateKey);
        return cipher.doFinal(encrypted);
    }

    public static void main(String[] args) throws NoSuchProviderException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, NoSuchPaddingException, InvalidKeyException {

        RSA rsa = new RSA();
        rsa.init();
        byte[] encrypted = rsa.encrypt("1".getBytes());
        byte[] decrypted = rsa.decrypt(encrypted);
        System.out.println(new String(decrypted));
        long t_enc = 0;
        long t_dec = 0;
        long t_xor = 0;

        t_enc -= new java.util.Date().getTime();
        for (int i = 0; i < 100000; ++i) {
            rsa.encrypt("1".getBytes());
        }
        t_enc += new java.util.Date().getTime();

        byte[] b1 = rsa.encrypt("1".getBytes());
        t_dec -= new java.util.Date().getTime();
        for (int i = 0; i < 10000; ++i) {
            rsa.decrypt(b1);
        }
        t_dec += new java.util.Date().getTime();

        byte[] b2 = rsa.encrypt("1".getBytes());
        t_xor -= new java.util.Date().getTime();
        for (int i = 0; i < 100000; ++i) {
            rsa.xor(b1, b2);
        }
        t_xor += new java.util.Date().getTime();

        System.out.println("# encryptions per second = " + String.valueOf(100000 * 1000 / (double) t_enc));
        System.out.println("# decryptions per second = " + String.valueOf(10000 * 1000 / (double) t_dec));
        System.out.println("# XORs per second = " + String.valueOf(100000 * 1000 / (double) t_xor));
    }

    private byte[] xor(byte[] b1, byte[] b2) {
        byte[] output = new byte[b1.length];
        if (b1.length == b2.length) {
            for (int i = 0; i < b1.length; i++) {
                output[i] = (byte) (b1[i] ^ b2[i]);

            }
        }
        return output;

    }
}
