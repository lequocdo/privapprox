import java.util.concurrent.TimeUnit
import java.util.Properties

//import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.api.windowing.windows.TimeWindow
import org.apache.flink.streaming.api.windowing.assigners.SlidingTimeWindows
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer082
import org.apache.flink.streaming.util.serialization.SimpleStringSchema

import org.apache.flink.streaming.api.windowing.assigners.TumblingTimeWindows

import axle._
import axle.stats._
import spire.math._
import spire.algebra._

object Aggregator {

  def main(args: Array[String]) {

    val env = StreamExecutionEnvironment.getExecutionEnvironment
    val properties = new Properties();
    properties.setProperty("bootstrap.servers", "stream32:9092,stream33:9092,stream34:9092,stream35:9092, stream36:9092");
    properties.setProperty("zookeeper.connect", "stream7:2181,stream8:2181,stream9:2181");
    properties.setProperty("group.id", "aggregator-test");

    val streamEncodedAnswer = env.addSource(new FlinkKafkaConsumer082[String]("Answer", new SimpleStringSchema(), properties))
      .map(x => (x.split(",")(0), x.split(",")(1)))
    val streamKey = env.addSource(new FlinkKafkaConsumer082[String]("Key", new SimpleStringSchema(), properties))
      .map(x => (x.split(",")(0), x.split(",")(1)))

    val streamAnswer = streamEncodedAnswer.join(streamKey)
      //.window(TumblingEventTimeWindows.of(Time.seconds(3)))
      .where(_._1)
      .equalTo(_._1)

      .window(TumblingTimeWindows.of(Time.seconds(1))) { (c1, c2) => {
        if (coin(Rational(9, 10)) == 'HEAD) {
          c1._2.toInt ^ c2._2.toInt
        }
      }
      }

    streamAnswer.print

    env.execute("Aggregator")
  }
}
