 import AssemblyKeys._

name := "flink-Aggregator"

version := "1.0"

scalaVersion := "2.11.4"

resolvers += "sonatype snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"

resolvers += "sonatype releases" at "https://oss.sonatype.org/content/repositories/releases/"

libraryDependencies  ++= Seq(
  "org.scalanlp" %% "breeze" % "0.11.2",
  "org.scalanlp" %% "breeze-natives" % "0.11.2",
  "org.scalanlp" %% "breeze-viz" % "0.11.2",
  "org.axle-lang" %% "axle-core" % "0.2.0"
)

libraryDependencies ++= Seq("org.apache.flink" % "flink-core_2.11" % "0.10.1", "org.apache.flink" % "flink-scala_2.11" % "0.10.1", "org.apache.flink" % "flink-clients_2.11" % "0.10.1", "org.apache.flink" % "flink-java_2.11" % "0.10.1", "org.apache.flink" % "flink-streaming-scala_2.11" % "0.10.1", "org.apache.flink" % "flink-connector-kafka_2.11" % "0.10.1" )

fork in run := true

assemblySettings

mergeStrategy in assembly := {
  case m if m.toLowerCase.endsWith("manifest.mf")          => MergeStrategy.discard
  case m if m.toLowerCase.matches("meta-inf.*\\.sf$")      => MergeStrategy.discard
  case "log4j.properties"                                  => MergeStrategy.discard
  case m if m.toLowerCase.startsWith("meta-inf/services/") => MergeStrategy.filterDistinctLines
  case "reference.conf"                                    => MergeStrategy.concat
  case _                                                   => MergeStrategy.first
}
