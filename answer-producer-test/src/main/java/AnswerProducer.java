
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.typeutils.TypeExtractor;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;
import org.apache.flink.streaming.util.serialization.DeserializationSchema;
import org.apache.flink.streaming.util.serialization.SerializationSchema;
import org.apache.flink.streaming.util.serialization.SimpleStringSchema;


public class AnswerProducer {
    public static void main(String[] args) throws Exception {
        // create execution environment
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        // parse user parameters
        ParameterTool parameterTool = ParameterTool.fromArgs(args);

        // add a simple source which is writing some strings
        DataStream<String> answerStream = env.addSource(new AnswerStringGenerator());
        DataStream<String> keyStream = env.addSource(new KeyStringGenerator());

        // write Answer stream to Kafka
        answerStream.addSink(new FlinkKafkaProducer<>(parameterTool.getRequired("bootstrap.servers"),
                parameterTool.getRequired("answer.topic"),
                new SimpleStringSchema()));

        //write Key stream to Kafka
        keyStream.addSink(new FlinkKafkaProducer<>(parameterTool.getRequired("bootstrap.servers"),
                parameterTool.getRequired("key.topic"),
                new SimpleStringSchema()));


        env.execute();
    }

    public static class AnswerStringGenerator implements SourceFunction<String> {
        private static final long serialVersionUID = 2174904787118597072L;
        boolean running = true;
        long i = 0;

        @Override
        public void run(SourceContext<String> ctx) throws Exception {
            while (running) {
                ctx.collect("1836E1591EE79F810104064B945747AB,1628");
                //Thread.sleep(10);
            }
        }

        @Override
        public void cancel() {
            running = false;
        }
    }

    public static class KeyStringGenerator implements SourceFunction<String> {
        private static final long serialVersionUID = 2199904787118597072L;
        boolean running = true;
        long i = 0;

        @Override
        public void run(SourceContext<String> ctx) throws Exception {
            while (running) {
                ctx.collect("1836E1591EE79F810104064B945747AB,1688");
                // Thread.sleep(10);
            }
        }

        @Override
        public void cancel() {
            running = false;
        }
    }

    public static class SimpleStringSchema implements DeserializationSchema<String>, SerializationSchema<String, byte[]> {
        private static final long serialVersionUID = 1L;

        public SimpleStringSchema() {
        }

        public String deserialize(byte[] message) {
            return new String(message);
        }

        public boolean isEndOfStream(String nextElement) {
            return false;
        }

        public byte[] serialize(String element) {
            return element.getBytes();
        }

        public TypeInformation<String> getProducedType() {
            return TypeExtractor.getForClass(String.class);
        }
    }
}


