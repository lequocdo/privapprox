### What is this repository for? ###

* PrivApprox prototype

### How do I get set up? ###

#### Run Flink ####
* $/path/to/flink/bin/start-cluster.sh

#### Run Proxies (kafka): ####
* $path/to/zookeeper/bin/zkServer.sh start

* $path/to/kafka/bin/kafka-server-start.sh  config/server.properties

* Start Producer:
  $cd path/to/privapprox/answer-producer 

* $flink run -c "AnswerProducer" target/privapprox-answer-generator-0.1.jar --answer.topic Answer --key.topic Key  --bootstrap.servers kafka-host:9092 --zookeeper.connect zk-host:2181 --group.id myGroup

#### Run Aggregator ####
* $cd /path/to/privapprox/aggregator
  
* $flink run -c "Aggregator" target/scala-2.11/flink-Aggregator-assembly-1.0.jar

#### Cluster setup ####
* One click deployment: $cd /path/to/script/cluster-setup/ && fab setupCluster

#### Historical Analytics ####
* Sampling operator is available at https://bitbucket.org/lequocdo/flink

### Who do I talk to? ###

* Do Le Quoc: do.le_quoc@tu-dresden.de
