from random import random
import math
from math import log


class Privacy(object):
    ''' Base class for privacy calculation '''

    def getRandomNum(self):
        p = 0
        while p == 0:
            p = random()
        return p

    def epsilonDifferentialPrivacy(self, p, q):
        '''Differential Privacy'''
        eps = log((p + (1 - p) * q) / ((1 - p) * q))
        return eps

    def epsilonZeroKnowledgePrivacy(self, s, p, q):
        '''Zero-knowledge Privacy'''
        eps = log(float(s * (2 - s) / (1 - s) * ((p + (1 - p) * q) / ((1 - p) * q))) + (1 - s))
        return eps

    def quadraticSolver(self, a, b, c):
        '''Quadratic Solver'''
        delta = b ** 2 - 4 * a * c  # discriminant
        if delta < 0:
            print ("This equation has no real solution")
            return 0
        elif delta == 0:
            x = (-b + math.sqrt(b ** 2 - 4 * a * c)) / 2 * a
            print ("This equation has one solutions: "), x
            return x
        else:
            x1 = (-b + math.sqrt((b ** 2) - (4 * (a * c)))) / (2 * a)
            x2 = (-b - math.sqrt((b ** 2) - (4 * (a * c)))) / (2 * a)
        if (x1 > 0) and (x1 < 1):
            return x1
        elif (x2 > 0) and (x2 < 1):
            return x2
        else:
            return 0

    def reverseS(self, eps, p, q):
        '''Calcualte sampling fraction to achieve eps zero-knowledge privacy
           with given randomized response parameters p, q.
        '''
        m = (p + (1 - p) * q) / ((1 - p) * q)
        a = 1 - m
        b = 2.71828 ** eps + 2 * m - 2
        c = 1 - 2.71828 ** eps
        s = self.quadraticSolver(a, b, c)
        return s
