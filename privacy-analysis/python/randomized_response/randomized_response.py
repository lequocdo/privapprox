from random import random
from random import shuffle


class RandomizedResponse(object):
    '''Base class for the randomized responese process'''

    def __init__(self):
        '''randomized response parameters'''
        self.p = None
        self.q = None

    def biasedCoin(self, p):
        '''biased sampling with proability is p'''
        return int(random() < p)

    def flipCoinProb(p, N):
        num_yes = int(p * N)
        num_no = N - num_yes
        flips = [1] * num_yes + [0] * num_no
        shuffle(flips)
        return flips

    def trueAnswerGenerator(self, N, y):
        '''generating truthful answers with Yes answers is y%'''
        num_yes = int(N * y)
        num_no = N - num_yes
        buckets = [0] * num_no + [1] * num_yes
        shuffle(buckets)
        return buckets

    def randomizeAnswer(self, answer, p, q):
        '''randominzed response process'''
        randomized_answer = []
        for i in range(len(answer)):
            # flip the first coin
            flips1 = self.biasedCoin(p)
            if flips1 == 1:
                randomized_answer.append(answer[i])
            else:
                # flip the second coin
                flips2 = self.biasedCoin(q)
                # print flips2
                if flips2 == 1:
                    # print flips2
                    randomized_answer.append(1)
                else:
                    randomized_answer.append(0)
        return randomized_answer

    def estimateAnswerYes(self, randomized_answer, p, q, num_answers):
        '''Estimating # Yes truthful answers after randomized response process'''
        num_yes = self.countYes(randomized_answer)
        estimate_y = (num_yes - (1 - p) * q * num_answers) / float(p)
        return estimate_y

    def estimateSampleAnswerYes(self, answer, s, p, q):
        '''Estimating # Yes truthful answers after
        random sampling and randomized response process
        '''
        estimate_y = self.estimateAnswerYes(answer, p, q, len(answer)) * (1 / s)
        return estimate_y

    def countYes(self, answer):
        '''Counting # Yes answers'''
        num_yes = 0
        for i in answer:
            if i == 1:
                num_yes = num_yes + 1
        return num_yes
