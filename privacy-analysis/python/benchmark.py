from randomized_response import RandomizedResponse
from privacy import Privacy
from random import random
import numpy


def sampleRandomizeAnswer(trueAnswer, s, p, q):
    '''Answering query'''
    rp = RandomizedResponse()
    sample_answer = [i for i in trueAnswer if random() < s]
    randomized_answer = rp.randomizeAnswer(sample_answer, p, q)
    return randomized_answer


def sampleRandomizeAnswerFile(filename, s, p, q, query_value):
    '''Answering query with clients from a input file'''
    lines = [line for line in open(filename) if random() < s]
    rp = RandomizedResponse()
    answer = []
    # Generate true answer
    for line in lines:
        value = line.split(",")[-1]
        if int(value) == query_value:
            answer.append(1)
        else:
            answer.append(0)
    randomized_answer = rp.randomizeAnswer(answer, p, q, len(answer))
    return randomized_answer


def hundredRunExperiement(filename, s, p, q, query_value, num_yes):
    rp = RandomizedResponse()
    error = 0
    for i in range(100):
        sampling_answer = sampleRandomizeAnswerFile(filename, s, p, q, query_value)
        estimate_y = rp.estimateSampleAnswerYes(sampling_answer, s, p, q)
        error = error + abs(((num_yes - estimate_y) / query_value) * 100)
    error = error / 100
    return error


def relativeError(self, filename, eps, query_value):
    priv = Privacy()
    p = priv.getRandomNum()
    q = priv.getRandomNum()
    sampling_rate = priv.reverseS(eps, p, q)
    error = self.hundredRunExperiements(filename, p, q, sampling_rate, query_value)
    print "Relative Error after 100 runs: ", error, "with epsilon:", eps
    return error


def runMicroBenchmark(s, p, q, N, y):
    rp = RandomizedResponse()
    answer = rp.trueAnswerGenerator(N, y)
    true_y = N * y
    error_list = []
    for i in range(100):
        sampling_answer = sampleRandomizeAnswer(answer, s, p, q)
        estimate_y = rp.estimateSampleAnswerYes(sampling_answer, s, p, q)
        error_list.append(abs(true_y - estimate_y) / true_y)
    error = numpy.mean(error_list)
    std = numpy.std(error_list)
    return error, std


if __name__ == '__main__':
    '''Test'''
    N = 10000  # number of clients
    s = 0.2
    p = 0.6
    q = 0.3
    y = 0.6  # Yes answer fraction
    error, std = runMicroBenchmark(s, p, q, N, y)
    print "s = ", s, " y = ", y, " p=", p, "q=", q, "N= ", N, " :", "Relative Error after 100 runs: ", error, "std:", std
