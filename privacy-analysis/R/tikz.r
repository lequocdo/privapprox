# generic variables to configure (tikz) output for A4 scrbook fitting

library(tikzDevice)
library(RColorBrewer)

textWidthVar <- 5.5
pointSizeVar <- 10
lwdVar <- 3
cexVar <- 1
cex.axisVar <- 0.9
axis.colorVar <- "darkgray"
lasVar <- 1

# SET MARGIN OFFSETS
marOffBottom <- 0
marOffLeft <- 0
marOffTop <- 0
marOffRight <- 0.1
marOffVec <- c(marOffBottom, marOffLeft, marOffTop, marOffRight)

figureColorsVar <- brewer.pal(9, "Set1")
pointTypesVar <- c(8,20,17,15,3)

# Returns (approximated) \textwidth in inch
textWidth <- function() {
  textWidthVar
}

# Returns pointSize for figures
pointSize <- function() {
  pointSizeVar
}

# Returns heigth in inch given textwidth
figureHeigthByAspectRatio <- function(aRx,aRy) {
  textWidth()*aRy/aRx
}

# Returns heigth in inch given textwidth
figureHeigthByAspectRatioUsingWidth <- function(width,x,y) {
  width*y/x
}

# Returns line width
lineWidth <- function() {
  lwdVar
}

# Returns label orientation
labelOrientation <- function() {
  lasVar
}

# Returns scaling of figure text and symbols
charScale <- function() {
  cexVar
}

# Returns small scaling of figure text and symbols
charScaleSmall <- function() {
  cexVar*0.8
}

# Returns small scaling of figure text and symbols
charScaleLarge <- function() {
  cexVar*1.2
}

axisScale <- function() {
  cex.axisVar
}

# Returns color for axis
axisColor <- function() {
  axis.colorVar
}

# Returns point types
pointTypes <- function() {
  pointTypesVar
}

# Returns common colors for figures
figureColors <- function() {
  figureColorsVar
}

# Returns common colors for figures
figureGrey <- function() {
  figureColors()[length(figureColors())]
}


# Starts a device for plotting
# Opens a tex file to plot tikz code into
startPlot <- function(filename, aRx, aRy, unclip=F) {
  startPlotWidth(filename = filename, aRx = aRx, aRy = aRy, width=NA, unclip=unclip)
}

# Starts a device for plotting
# Opens a tex file to plot tikz code into
startPlotHalf <- function(filename, aRx, aRy, unclip=F) {
  startPlotWidth(filename = filename, aRx = aRx, aRy = aRy, width=(textWidth()*0.9)/2, unclip=unclip )
}

# Starts a device for plotting given a specific width
# Opens a tex file to plot tikz code into
startPlotWidth <- function(filename, aRx, aRy, width, unclip=F) {
  unclipVar <- ""
  par(mar=par("mar")+marOffVec)
  if(unclip) {
    unclipVar <- "%UnClip"
  }

  if(is.na(width)) {
    tikz(filename, width=textWidth(), height=figureHeigthByAspectRatio(aRx,aRy), pointsize=pointSize(), standAlone = T, footer=unclipVar)
  } else {
    tikz(filename, width=width, height=figureHeigthByAspectRatioUsingWidth(width,aRx,aRy), pointsize=pointSize(), standAlone = T, footer=unclipVar)
  }
  dev.cur()
}

# Closes tex file and device to finish plotting
stopPlot <- function(deviceNum) {
  dev.off(which = deviceNum)
  par(mar=par("mar")-marOffVec)
}

# Plots horizontal line(s) as cut off value
yCut <- function(y) {
  abline(h=y, lty=2, col=axisColor())
}

# Plots vertical line(s) as cut off value
xCut <- function(x) {
  abline(v=x, lty=2, col=axisColor())
}

# Draws the bottom axis
axis1 <- function(labels, labelPositions) {
  axis(1, cex.axis = axisScale(), col = axisColor(), las=labelOrientation(), labels = labels, at = labelPositions)
}

# Draws the left axis
axis2 <- function(labels, labelPositions) {
  axis(2, cex.axis = axisScale(), col = axisColor(), las=labelOrientation(), labels = labels, at = labelPositions)
}

# Draws the top axis
axis3 <- function(labels, labelPositions) {
  axis(3, cex.axis = axisScale(), col = axisColor(), las=labelOrientation(), labels = labels, at = labelPositions)
}

# Draws the right axis
axis4 <- function(labels, labelPositions) {
  axis(4, cex.axis = axisScale(), col = axisColor(), las=labelOrientation(), labels = labels, at = labelPositions)
}