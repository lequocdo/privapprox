source(file = "./tikz.r")

epsilonRR <- function(p,q) {
  eRR <- (p+(1-p)*q)/((1-p)*q)
  eRR
}

epsilonZKSampling <- function(prob, e) {
  eSampl <- (2-prob)/(1-prob) * prob * e -prob + 1
  eSampl
}

epsilonDPSampling <- function(prob, e) {
  eSampl <- prob * e -prob + 1
  eSampl
}

epsilonZKCombined <- function(p,q,r) {
  eZKPost <- epsilonZKSampling(r,epsilonRR(p,q))
  eZKPost
}

epsilonDPCombined <- function(p,q,r) {
  eDPPost <- epsilonDPSampling(r,epsilonRR(p,q))
  eDPPost
}

## Randomized Response ##
# Biased coin p
# Probability truthful answer coin comes head
p <- runif(n=1)

# Biased coin q
# Probability non-truthful answer "yes"-coin comes head
q <- runif(n=1)

## Sampling ##
# Biased coin r - pre-sampling at client side
# Probability to take part in survey
r <- runif(n=1)
r <- 0.1

# Biased coin s - post-sampling at aggregator side
# Probability to include result
s <- runif(n=1)
s <- 0.2


eRR <- log(epsilonRR(p,q))
eZK <- log(epsilonZKCombined(p,q,r))
eDP <- log(epsilonDPCombined(p,q,r))
resolution <- 100

spaceRR <- array(NA, dim=c(resolution-1, resolution-1))
spaceZK <- array(NA, dim=c(resolution-1, resolution-1, resolution-1))
spaceDP <- array(NA, dim=c(resolution-1, resolution-1, resolution-1))

step <- (1/resolution)
step_seq <- seq(from=0+step, to=1-step, by=step)
for(p in step_seq) {
  pIdx <- round(p*resolution)
  for(q in step_seq) {
    qIdx <- round(q*resolution)
    for(r in step_seq) {
      rIdx <- round(r*resolution)
      spaceZK[pIdx, qIdx, rIdx] <- log(epsilonZKCombined(p, q, r))
      spaceDP[pIdx, qIdx, rIdx] <- log(epsilonDPCombined(p, q, r))
    }
    spaceRR[pIdx,qIdx] <- log(epsilonRR(p,q))
  }
}

spaceDiff <- spaceZK-spaceDP
spaceRatio <- spaceZK/spaceDP

probs <- c(0.1,0.3,0.5,0.7,0.9)
legend_cols <- 2
yLayoutHeights <- c(14,3)
plotRatio <- 1/1
pageWidth <- 4
## differential privacy plots ##
pVals <- floor(probs*resolution)
ylim <- c(min(spaceDP[,min(pVals), min(pVals)]), max(spaceDP[,max(pVals), max(pVals)]))
yVals <- seq(from=min(floor(ylim)), to=max(ceiling(ylim)))
pID <- startPlotWidth(filename = "~/work/projekte/approxPrivacy/approvacy/papers/draft/figuresTikZ/eDP_p.tex", aRx = plotRatio, aRy = 1, width = pageWidth)
layout(rbind(1,2), heights=yLayoutHeights)  # put legend on bottom 1/8th of the chart
plot(NA, xlim = c(0,1), ylim = ylim, main = "\\begin{tabular}{c}\\(\\epsilon_{dp}\\)-differential privacy\\\\ {\\footnotesize dependency on \\(p\\)}\\end{tabular}", ylab="\\(\\epsilon_{dp}\\)", xlab="\\(p\\)", bty="n")
abline(h = yVals, col = figureGrey(), lty=3)
for(val in pVals) {
  lines(x = step_seq, y = spaceDP[,val,val], type="l", ylab="\\(\\epsilon_{dp}\\)", col = figureColors()[which(pVals == val)])
}
# setup for no margins on the legend
par(mar=c(0, 0, 0, 0))
# c(bottom, left, top, right)
plot.new()
legend("center", "groups", title = "Values \\(q,s\\) are fixed at:", legend = paste("\\(q=s=",probs,"\\)",sep=""), col = figureColors()[1:length(pVals)],lty = 1, bty="n", ncol=legend_cols)
stopPlot(pID)

qVals <- floor(probs*resolution)
ylim <- c(min(spaceDP[min(qVals), ,min(qVals)]), max(spaceDP[max(qVals), ,max(qVals)]))
yVals <- seq(from=min(floor(ylim)), to=max(ceiling(ylim)))
pID <- startPlotWidth(filename = "~/work/projekte/approxPrivacy/approvacy/papers/draft/figuresTikZ/eDP_q.tex", aRx = plotRatio, aRy = 1, width = pageWidth)
layout(rbind(1,2), heights=yLayoutHeights)  # put legend on bottom 1/8th of the chart
plot(NA, xlim = c(0,1), ylim = ylim, main = "\\begin{tabular}{c}\\(\\epsilon_{dp}\\)-differential privacy\\\\ {\\footnotesize dependency on \\(q\\)}\\end{tabular}", ylab="\\(\\epsilon_{dp}\\)", xlab="\\(q\\)", bty="n")
abline(h = yVals, col = figureGrey(), lty=3)
for(val in qVals) {
  lines(x = step_seq, y = spaceDP[val,,val], type="l", ylab="\\(\\epsilon_{dp}\\)", col = figureColors()[which(qVals == val)])
}
# setup for no margins on the legend
par(mar=c(0, 0, 0, 0))
# c(bottom, left, top, right)
plot.new()
legend("center", "groups", title = "Values \\(p,s\\) are fixed at:", legend = paste("\\(p=s=",probs,"\\)",sep=""), col = figureColors()[1:length(qVals)],lty = 1, bty="n", ncol=legend_cols)
stopPlot(pID)

rVals <- floor(probs*resolution)
ylim <- c(min(spaceDP[min(rVals),min(rVals),]), max(spaceDP[max(rVals), max(rVals),]))
yVals <- seq(from=min(floor(ylim)), to=max(ceiling(ylim)))
pID <- startPlotWidth(filename = "~/work/projekte/approxPrivacy/approvacy/papers/draft/figuresTikZ/eDP_r.tex", aRx = plotRatio, aRy = 1, width = pageWidth)
layout(rbind(1,2), heights=yLayoutHeights)  # put legend on bottom 1/8th of the chart
plot(NA, xlim = c(0,1), ylim = ylim, main = "\\begin{tabular}{c}\\(\\epsilon_{dp}\\)-differential privacy\\\\ {\\footnotesize dependency on \\(s\\)}\\end{tabular}", ylab="\\(\\epsilon_{dp}\\)", xlab="\\(s\\)", bty="n")
abline(h = yVals, col = figureGrey(), lty=3)
for(val in rVals) {
  lines(x = step_seq, y = spaceDP[val,val,], type="l", ylab="\\(\\epsilon_{dp}\\)", col = figureColors()[which(rVals == val)])
}
# setup for no margins on the legend
par(mar=c(0, 0, 0, 0))
# c(bottom, left, top, right)
plot.new()
legend("center", "groups", title = "Values \\(p,q\\) are fixed at:", legend = paste("\\(p=q=",probs,"\\)",sep=""), col = figureColors()[1:length(rVals)],lty = 1, bty="n", ncol=legend_cols)
stopPlot(pID)

## zero-knowledge privacy plots ##
pVals <- floor(probs*resolution)
ylim <- c(min(spaceZK[,min(pVals), min(pVals)]), max(spaceZK[,max(pVals), max(pVals)]))
yVals <- seq(from=min(floor(ylim)), to=max(ceiling(ylim)))
pID <- startPlotWidth(filename = "~/work/projekte/approxPrivacy/approvacy/papers/draft/figuresTikZ/eZK_p.tex", aRx = plotRatio, aRy = 1, width = pageWidth)
layout(rbind(1,2), heights=yLayoutHeights)  # put legend on bottom 1/8th of the chart
plot(NA, xlim = c(0,1), ylim = ylim, main = "\\begin{tabular}{c}\\(\\epsilon_{zk}\\)-zero-knowledge privacy\\\\ {\\footnotesize dependency on \\(p\\)}\\end{tabular}", ylab="\\(\\epsilon_{zk}\\)", xlab="\\(p\\)", bty="n")
abline(h = yVals, col = figureGrey(), lty=3)
for(val in pVals) {
  lines(x = step_seq, y = spaceZK[,val,val], type="l", ylab="\\(\\epsilon_{zk}\\)", col = figureColors()[which(pVals == val)])
}
# setup for no margins on the legend
par(mar=c(0, 0, 0, 0))
# c(bottom, left, top, right)
plot.new()
legend("center", "groups", title = "Values \\(q,s\\) are fixed at:", legend = paste("\\(q=s=",probs,"\\)",sep=""), col = figureColors()[1:length(pVals)],lty = 1, bty="n", ncol=legend_cols)
stopPlot(pID)

qVals <- floor(probs*resolution)
ylim <- c(min(spaceZK[min(qVals), ,min(qVals)]), max(spaceZK[max(qVals), ,max(qVals)]))
yVals <- seq(from=min(floor(ylim)), to=max(ceiling(ylim)))
pID <- startPlotWidth(filename = "~/work/projekte/approxPrivacy/approvacy/papers/draft/figuresTikZ/eZK_q.tex", aRx = plotRatio, aRy = 1, width = pageWidth)
layout(rbind(1,2), heights=yLayoutHeights)  # put legend on bottom 1/8th of the chart
plot(NA, xlim = c(0,1), ylim = ylim, main = "\\begin{tabular}{c}\\(\\epsilon_{zk}\\)-zero-knowledge privacy\\\\ {\\footnotesize dependency on \\(q\\)}\\end{tabular}", ylab="\\(\\epsilon_{zk}\\)", xlab="\\(q\\)", bty="n")
abline(h = yVals, col = figureGrey(), lty=3)
for(val in qVals) {
  lines(x = step_seq, y = spaceZK[val,,val], type="l", ylab="\\(\\epsilon_{zk}\\)", col = figureColors()[which(qVals == val)])
}
# setup for no margins on the legend
par(mar=c(0, 0, 0, 0))
# c(bottom, left, top, right)
plot.new()
legend("center", "groups", title = "Values \\(p,s\\) are fixed at:", legend = paste("\\(p=s=",probs,"\\)",sep=""), col = figureColors()[1:length(qVals)],lty = 1, bty="n", ncol=legend_cols)
stopPlot(pID)

rVals <- floor(probs*resolution)
ylim <- c(min(spaceZK[min(rVals),min(rVals),]), max(spaceZK[max(rVals), max(rVals),]))
yVals <- seq(from=min(floor(ylim)), to=max(ceiling(ylim)))
pID <- startPlotWidth(filename = "~/work/projekte/approxPrivacy/approvacy/papers/draft/figuresTikZ/eZK_r.tex", aRx = plotRatio, aRy = 1, width = pageWidth)
layout(rbind(1,2), heights=yLayoutHeights)  # put legend on bottom 1/8th of the chart
plot(NA, xlim = c(0,1), ylim = ylim, main = "\\begin{tabular}{c}\\(\\epsilon_{zk}\\)-zero-knowledge privacy\\\\ {\\footnotesize dependency on \\(s\\)}\\end{tabular}", ylab="\\(\\epsilon_{zk}\\)", xlab="\\(s\\)", bty="n")
abline(h = yVals, col = figureGrey(), lty=3)
for(val in rVals) {
  lines(x = step_seq, y = spaceZK[val,val,], type="l", ylab="\\(\\epsilon_{zk}\\)", col = figureColors()[which(rVals == val)])
}
# setup for no margins on the legend
par(mar=c(0, 0, 0, 0))
# c(bottom, left, top, right)
plot.new()
legend("center", "groups", title = "Values \\(p,q\\) are fixed at:", legend = paste("\\(p=q=",probs,"\\)",sep=""), col = figureColors()[1:length(rVals)],lty = 1, bty="n", ncol=legend_cols)
stopPlot(pID)

rVals <- floor(probs*resolution)
ylim <- c(min(spaceRatio[min(rVals),min(rVals),]), max(spaceRatio[max(rVals), max(rVals),]))
ylim <- c(1,7)
yVals <- seq(from=min(floor(ylim)), to=max(ceiling(ylim)))
pID <- startPlotWidth(filename = "~/work/projekte/approxPrivacy/approvacy/papers/draft/figuresTikZ/eZK_eDP.tex", aRx = plotRatio, aRy = 1, width = pageWidth)
layout(rbind(1,2), heights=yLayoutHeights)  # put legend on bottom 1/8th of the chart
plot(NA, xlim = c(0,1), ylim = ylim, main = "\\begin{tabular}{c}Ratio between zero-knowledge privacy\\\\ and differential privacy \\end{tabular}", ylab="\\(\\frac{\\epsilon_{zk}}{\\epsilon_{dp}}\\)", xlab="\\(s\\)", bty="n")
abline(h = yVals, col = figureGrey(), lty=3)
for(val in rVals) {
  lines(x = step_seq, y = spaceRatio[val,val,], type="l", col = figureColors()[which(rVals == val)])
}
# setup for no margins on the legend
par(mar=c(0, 0, 0, 0))
# c(bottom, left, top, right)
plot.new()
legend("center", "groups", title = "Values \\(p,q\\) are fixed at:", legend = paste("\\(p=q=",probs,"\\)",sep=""), col = figureColors()[1:length(rVals)],lty = 1, bty="n", ncol=legend_cols)
stopPlot(pID)


## same plot with legend inside plot ##
rVals <- floor(probs*resolution)
ylim <- c(min(spaceRatio[min(rVals),min(rVals),]), max(spaceRatio[max(rVals), max(rVals),]))
ylim <- c(1,7)
yVals <- seq(from=min(floor(ylim)), to=max(ceiling(ylim)))
pID <- startPlotWidth(filename = "~/work/projekte/approxPrivacy/approvacy/papers/vldb17/figuresTikZ/eZK_eDP.tex", aRx = plotRatio, aRy = 0.6, width = pageWidth)
#plot(NA, xlim = c(0,1), ylim = ylim, main = "\\begin{tabular}{c}Ratio between zero-knowledge privacy\\\\ and differential privacy \\end{tabular}", ylab="\\(Privacy\\;Ratio\\;\\frac{\\epsilon_{zk}}{\\epsilon_{dp}}\\)", xlab="Sampling fraction \\(s\\) (\\%)", bty="n")
#oldMar <- par("mar")
#oldOma <- par("oma")
oldMar <- c(5.1,4.1,4.1,2.1)
oldOma <- c(2,2,2,2)
par(mar=c(5.1, 4.1, 1, 0))
par(oma=c(0,0,0,0))
plot(NA, xlim = c(0,1), ylim = ylim, ylab="\\(Privacy\\;ratio\\;\\frac{\\epsilon_{zk}}{\\epsilon_{dp}}\\)", xlab="Sampling fraction \\(s\\) (\\%)", bty="n")
abline(h = yVals, col = figureGrey(), lty=3)
legend(x=0.02,y=6.8, title = "Values \\(p,q\\) are fixed at:", legend = paste("\\(p=q=",probs,"\\)",sep=""), col = figureColors()[1:length(rVals)],lty = 1, ncol=legend_cols, box.lwd = 0,box.col = "white",bg = "white")
for(val in rVals) {
  lines(x = step_seq, y = spaceRatio[val,val,], type="l", col = figureColors()[which(rVals == val)])
}
# setup for no margins on the legend
# c(bottom, left, top, right)
par(mar=oldMar)
par(oma=oldOma)
stopPlot(pID)


### EXPORT DATA ###

colnames(spaceRatio)
