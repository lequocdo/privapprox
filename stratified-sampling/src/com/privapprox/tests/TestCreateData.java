package com.privapprox.tests;

import com.privapprox.core.Stratified;
import com.privapprox.generateStreams.SimulateData;

public class TestCreateData extends Stratified {
    public static void main(String[] args) throws Throwable {

		/*
         * auto-generated poisson streams with fixed arrival rate in the format
		 * "stratum:arrival_rate:time"
		 */

        SimulateData sm = new SimulateData();
        sm.create_poissonData();
        System.out
                .println("Poisson data stream saved in ./input/data_file.txt");
    }
}