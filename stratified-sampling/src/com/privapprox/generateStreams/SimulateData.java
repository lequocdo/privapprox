package com.privapprox.generateStreams;

import java.io.File;
import java.util.ArrayList;

public class SimulateData {
    public void createPoissonData() throws Throwable {

		/*
         * Poisson streams are generated in these files
		 */
        File s1_file = new File("./input/s1_pois.txt");
        File s2_file = new File("./input/s2_pois.txt");
        File s3_file = new File("./input/s3_pois.txt");

        File dataFile = new File("./input/data_file.txt");

        ArrayList<File> lst_input = new ArrayList<File>();
        lst_input.add(s1_file);
        lst_input.add(s2_file);
        lst_input.add(s3_file);

        ArrayList<String> lst_strata = new ArrayList<String>();
        lst_strata.add("s1");
        lst_strata.add("s2");
        lst_strata.add("s3");

        ArrayList<Integer> lst_arrival_rates = new ArrayList<Integer>();
        lst_arrival_rates.add(3);
        lst_arrival_rates.add(5);
        lst_arrival_rates.add(5);

        GeneratePoisson gen = new GeneratePoisson();

        gen.generate(lst_strata, lst_input, lst_arrival_rates, dataFile);
    }
}