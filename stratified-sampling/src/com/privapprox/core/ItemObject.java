package com.privapprox.core;

import java.util.Comparator;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class ItemObject implements Comparator<ItemObject>,
        Comparable<ItemObject> {
    String strata;
    int item;
    String timestamp;

    ItemObject() {
    }

    ItemObject(String s, int i, String t) {
        strata = s;
        item = i;
        timestamp = t;
    }

    protected String get() {
        String s = null;
        s = strata.concat(":").concat(String.valueOf(item)).concat(":")
                .concat(timestamp);
        return s;
    }

    protected ItemObject set(String s) {
        strata = s.substring(0, s.indexOf(":"));
        item = Integer.valueOf(s.substring(s.indexOf(":") + 1,
                s.lastIndexOf(":")));
        timestamp = s.substring(s.lastIndexOf(":") + 1, s.length());
        return this;
    }

    // timestamp returned as string for ease of compareTo function
    // user must convert to long and use..
    protected String getTimestamp() {
        return timestamp;
    }

    protected int getDataItem() {
        return item;
    }

    protected String getStrata() {
        return strata;
    }

    @Override
    public int compareTo(ItemObject o) {
        return (this.getTimestamp()).compareTo(o.getTimestamp());
    }

    @Override
    public int compare(ItemObject o1, ItemObject o2) {
        // TODO;
        return 0;

    }

    public void clear() {
        strata = null;
        item = 0;
        timestamp = null;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
                // if deriving: appendSuper(super.hashCode()).
                        append(strata).append(item).append(timestamp).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof ItemObject))
            return false;
        if (obj == this)
            return true;

        ItemObject rhs = (ItemObject) obj;
        return new EqualsBuilder()
                .
                // if deriving: appendSuper(super.equals(obj)).
                        append(strata, rhs.strata).append(item, rhs.item)
                .append(timestamp, rhs.timestamp).isEquals();
    }
}
