package com.privapprox.core.Logging;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class ReadLog {

    /* Read Log and generate summary for plot - in output.data file */
    public void read() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(
                "./intermediate_results/fluctuating_arrival.txt/"));

        File outputFile = new File("./output/output.dat");

        BufferedWriter output = new BufferedWriter(new FileWriter(outputFile));

        String s = null;
        reader.readLine();
        s = reader.readLine();
        // just write first line (column names)
        if (s != null) {
            output.write(s);
        }
        int x = 3, y = 0, i = 0;

        ArrayList<String> lst = new ArrayList<String>();
        lst.add("1:5");
        lst.add("3:5");
        lst.add("2:4");
        lst.add("4:2");
        lst.add("1:6");
        while (x <= 402) {
            if ((reader.readLine()) != null) {
                s = reader.readLine();
                y++;
                if (y == 100) {
                    s.concat("\t").concat(lst.get(i));
                    output.write(s);
                    y = 0;
                    i++;
                }
                x++;
            }
        }

        reader.close();
        output.close();
    }
}