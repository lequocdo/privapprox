package com.privapprox.core.Logging;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Logging {

    private static BufferedWriter vary_log, fixed_log, fluctuate_log;
    private static BufferedReader reader;

    public void initializeVaryingWindowLog(ArrayList<ArrayList> t)
            throws IOException {
        vary_log = new BufferedWriter(new FileWriter(
                "./intermediate_results/vary_log.txt"));

        vary_log.flush();
        vary_log.write("S1\tS2\tS3\tTOTAL\tsampleNeeded\tChangeInWindow");
        vary_log.write("\n");
        // Write zero as first row - to show that memoized items for re-used by
        // first window is 0
        vary_log.write("0\t0\t0\t0\t".concat(t.get(0).get(0).toString())
                .concat("\t").concat(t.get(1).get(0).toString()));

        vary_log.write("\n");
        writeVaryingWindowLog(t);
    }

    public void initializeFixedWindows() throws IOException {
        fixed_log = new BufferedWriter(new FileWriter(
                "./intermediate_results/logSummary.txt", true));
        fixed_log.flush();
        fixed_log.write("S1\tS2\tS3\tTOTAL");
        fixed_log.close();
    }

    public void writeVaryingWindowLog(ArrayList<ArrayList> t)
            throws IOException {
        String str = null;
        int i = 0;// number of lines read from window
        reader = new BufferedReader(new FileReader(
                "./intermediate_results/logSummary.txt"));

        reader.readLine();// neglect first line (title)
        i++;

        while ((str = reader.readLine()) != null && i < t.get(0).size()
                && i < t.get(1).size()) {
            String d = t.get(0).get(i).toString();
            String e = t.get(1).get(i).toString();
            if (Integer.valueOf(e) > 0) {
                e = "+".concat(e);
            }
            str = str.concat("\t").concat(d).concat("\t").concat(e);
            i++;
            if (vary_log != null) {
                vary_log.write(str);
                vary_log.write("\n");
            }
        }
        vary_log.close();
        reader.close();
    }

    public void initializeFluctuatingWindowLog() throws IOException {
        BufferedWriter final_log = new BufferedWriter(new FileWriter(
                "./intermediate_results/logSummary.txt", true));
        final_log.write("S1\tS2\tS3\tTOTAL");
        final_log.write("\n");
        final_log.close();

        fluctuate_log = new BufferedWriter(new FileWriter(
                "./intermediate_results/fluctuating_arrival.txt", true));
        fluctuate_log.flush();
        fluctuate_log.write("S1\tS1New\ts1prev\tS2\tS2New\ts2prev\tS3\tS3New\ts3prev");
        fluctuate_log.write("\n");
        fluctuate_log.close();

    }
}