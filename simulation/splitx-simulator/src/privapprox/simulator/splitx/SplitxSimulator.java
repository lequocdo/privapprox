package privapprox.simulator.splitx;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.math3.distribution.EnumeratedIntegerDistribution;

import com.google.common.collect.Sets;


public class SplitxSimulator {

    public static String flip(double bias) {
        double current = Math.random();
        if (current < bias) {
            return "HEAD";
        } else {
            return "TAIL";
        }
    }

    public static List<Boolean> getAnswers_random(double yes_probability, int bucket_size) {
        List<Boolean> actual_answers = new ArrayList<Boolean>();
        int[] actual_values = new int[]{0, 1};
        double[] actual_probabilites = new double[]{1 - yes_probability, yes_probability};

        EnumeratedIntegerDistribution actual_distribution =
                new EnumeratedIntegerDistribution(actual_values, actual_probabilites);

        int[] actual = actual_distribution.sample(bucket_size);

        for (int x = 0; x < bucket_size; x++) {
            if (actual[x] == 1) {
                actual_answers.add(true);
            } else {
                actual_answers.add(false);
            }
        }
        return actual_answers;
    }

    public static List<Boolean> getAnswers(double yes_probability, int bucket_size) {
        int middle = (int) (bucket_size * yes_probability);
        List<Boolean> actual_answers = new ArrayList<Boolean>();

        for (int i = 0; i < middle; i++) {
            actual_answers.add(true);
        }

        for (int i = 0; i < (bucket_size - middle); i++) {
            actual_answers.add(false);
        }


        Collections.shuffle(actual_answers);
        return actual_answers;
    }

    public static Set<List<Boolean>> setGenerator(int num_clients) {
        Set<List<Boolean>> s = new HashSet<List<Boolean>>();

        for (int i = 0; i < num_clients; i++) {
            List<Boolean> answer = getAnswers(0.8, 64); //150, 384
            s.add(answer);
        }

        return s;
    }

    // Calculating Intersection of two Set S1, S2
    public static Set<List<Boolean>> getIntersection(Set<List<Boolean>> s1, Set<List<Boolean>> s2) {
        Set<List<Boolean>> intersection = Sets.intersection(s1, s2);

        return intersection;

    }

    // Calculating S1 - S2
    public static Set<List<Boolean>> getSubstract(Set<List<Boolean>> s1, Set<List<Boolean>> s2) {
        return Sets.symmetricDifference(s1, s2);
    }

    //Shuffle
    public static Set<List<Boolean>> doShuffle(Set<List<Boolean>> set) {
        Set<List<Boolean>> s = new HashSet<List<Boolean>>();
        Object[] a = new Object[64];

        for (int i = 0; i < 64; i++) {
            a[i] = new ArrayList<Boolean>();

        }

        for (Iterator iterator = set.iterator(); iterator.hasNext(); ) {
            List<Boolean> list = (List<Boolean>) iterator.next();
            for (Boolean element : list) {
                for (int i = 0; i < 64; i++) {

                    //System.out.println("Shuffle:" + element);
                    ((List<Boolean>) a[i]).add(element);
                }

            }

        }
        for (int i = 0; i < 64; i++) {
            Collections.shuffle((List<Boolean>) a[i]);
            s.add((List<Boolean>) a[i]);
        }

        return s;

    }

    public static void main(String[] args) {

        int num_clients = 10000000;
        int num_overlap = num_clients * 5 / 1000;
        Set<List<Boolean>> s1 = setGenerator(num_clients);
        Set<List<Boolean>> s2 = setGenerator(num_clients - num_overlap);

        int i = 0;
        for (Iterator iterator = s1.iterator(); iterator.hasNext(); ) {
            List<Boolean> list = (List<Boolean>) iterator.next();
            if (i < num_overlap) {

                //System.out.println(list.toString());
                s2.add(list);
                i++;
            } else {
                break;
            }
        }

        long t0 = System.nanoTime();
        // intersection S2'
        Set<List<Boolean>> intersec0 = getIntersection(s1, s2);

        //S1 - S2
        Set<List<Boolean>> s3 = getSubstract(s1, s2);

        //S1'
        Set<List<Boolean>> intersec1 = getSubstract(s1, s3);

        long t1 = System.nanoTime();
        //Shuffle
        Set<List<Boolean>> shuffle = doShuffle(intersec0);

        long t2 = System.nanoTime();

        System.out.println("#Clients:" + num_clients);
        System.out.println("Computation time: " + (t1 - t0) / 1000000 + " miliseconds");
        System.out.println("Shuffle time: " + (t2 - t1) / 1000000 + " miliseconds");
        System.out.println("Total time: " + (t2 - t0) / 1000000 + " miliseconds");

        for (Iterator iterator = intersec0.iterator(); iterator.hasNext(); ) {
            List<Boolean> list = (List<Boolean>) iterator.next();
            for (Boolean element : list) {
                //System.out.println("Result:" + element);
            }
        }


    }

}

