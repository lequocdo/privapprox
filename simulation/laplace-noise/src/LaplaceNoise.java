package privapprox.simulator.LaplaceNoise;

import java.security.SecureRandom;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Random;

public class LaplaceNoise {

    private final SecureRandom random = new SecureRandom();

    //Laplace Noise mechanism
    private static double laplace(double lambda) {
        Random r = new Random();
        double u = r.nextDouble() * -1 + 0.5;
        return -1
                * (lambda * Math.signum(u) * Math.log(1 - 2 * Math
                .abs(u)));
    }


    //PrivApprox
    public static BitSet fromByteArray(byte[] bytes, int bucket_size) {
        BitSet bs = new BitSet();
        for (int i = 0; i < bucket_size; i++) {
            int index = i / 8;
            int val = (bytes[index] & (1 << (i % 8)));
            if (val > 0) {
                //System.out.println("i="+i+" index="+index+" val="+val);
                bs.set(i);
            }
        }
        return bs;
    }


    private class XorEnrypt {
        public byte[] encryptedBytes;
        public byte[] keyBytes;
    }


    public static byte[] toByteArray(BitSet bits, int bucketsize) {
        int bytearraylength = (bucketsize + 7) / 8;
        byte[] bytes = new byte[bytearraylength];
        Arrays.fill(bytes, (byte) 0);
        for (int i = 0; i < bucketsize; i++) {
            if (bits.get(i)) {
                int index = i / 8;
                bytes[index] |= 1 << (i % 8);
            }
        }
        return bytes;
    }

    public XorEnrypt xorencryptAnswerBits(byte[] plaintext, int bucket_size) {
        final BitSet readonlyplaintextBitSet = fromByteArray(plaintext, bucket_size);

        BitSet encryptedBitSet = (BitSet) readonlyplaintextBitSet.clone();
        int length = plaintext.length;

        //generate key
        byte[] random_bytes = new byte[length];
        random.nextBytes(random_bytes);
        BitSet keyBitSet = fromByteArray(random_bytes, bucket_size);

        //encrypt
        encryptedBitSet.xor(keyBitSet);

        byte[] encryptedBytes = toByteArray(encryptedBitSet, bucket_size);
        byte[] keyBytes = toByteArray(keyBitSet, bucket_size);

        XorEnrypt xorEnrypt = new XorEnrypt();
        xorEnrypt.encryptedBytes = encryptedBytes;
        xorEnrypt.keyBytes = keyBytes;

        return xorEnrypt;
    }

    public static void main(String[] args) {
        long num_clients = 1000;
        int answer_length = 10000;

        byte[] random_bytesK = new byte[answer_length];
        BitSet keyBitSet = fromByteArray(random_bytesK, answer_length);

        byte[] random_bytesA = new byte[answer_length];
        BitSet answerBitSet = fromByteArray(random_bytesA, answer_length);

        //PrivaApprox
        long t0 = System.nanoTime();
        for (int i = 0; i <= num_clients; i++) {
            answerBitSet.xor(keyBitSet);
        }
        long t1 = System.nanoTime();

        //Laplace Noise
        long t2 = System.nanoTime();
        double lambda = 0.01;
        for (int i = 0; i <= num_clients; i++) {
            laplace(lambda);
        }
        long t3 = System.nanoTime();

        System.out.println("#Clients:" + num_clients);
        System.out.println("PrivApprox: " + (t1 - t0) / 1000000 + " miliseconds");
        System.out.println("Laplace noise based systems: " + (t3 - t2) / 1000000 + " miliseconds");

    }


}
